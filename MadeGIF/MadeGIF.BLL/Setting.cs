﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadeGIF.BLL
{
    public partial class Setting : Form
    {
        /// <summary>
        /// 设置保存动作
        /// </summary>
        public Action<dynamic> SettingSaveEvent { get; set; }
        public Setting()
        {
            InitializeComponent();
            fps.Text = Properties.Settings1.Default.FPS.ToString();
            quality.Text = Properties.Settings1.Default.Quality.ToString();
            savePath.Text = Properties.Settings1.Default.SavePath;
            repeat.Checked = Properties.Settings1.Default.Repeat;
            textMark.Text = Properties.Settings1.Default.TextMark;
            textMarkColor.Text = Properties.Settings1.Default.TextMarkColor;

            var toolTip1 = new ToolTip();
            toolTip1.SetToolTip(textMarkColor, @"请输入正确存在的颜色名称(eg:red、white、black)");
            toolTip1.SetToolTip(fps, @"数值越大，GIF播放越快（默认值24）");
            toolTip1.SetToolTip(quality, @"数值大，画质好，生成速度慢；数值小，画质差，生成速度快");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择GIF保存的位置";
            if (savePath.Text != string.Empty) dialog.SelectedPath = savePath.Text;
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                savePath.Text = dialog.SelectedPath.Trim();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(int.TryParse(fps.Text,out int tempFPS) && tempFPS<=0)
            {
                MessageBox.Show("帧数请输入大于0的整数（建议24/60/120）");
                return;
            }
            if (int.TryParse(quality.Text, out int tempQuality) && (tempQuality < 1 || tempQuality>100))
            {
                MessageBox.Show("画质请输入1~100内的整数");
                return;
            }
            SettingSaveEvent?.Invoke(new {
                FPS = tempFPS,
                Quality = quality.Text,
                SavePath = savePath.Text,
                Repeat= repeat.Checked,
                TextMark= textMark.Text,
                TextMarkColor= textMarkColor.Text
            });
        }
    }
}
