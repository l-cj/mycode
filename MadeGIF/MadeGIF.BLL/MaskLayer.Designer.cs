﻿namespace MadeGIF.BLL
{
    partial class MaskLayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sketchpad = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.sketchpad)).BeginInit();
            this.SuspendLayout();
            // 
            // sketchpad
            // 
            this.sketchpad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sketchpad.Location = new System.Drawing.Point(0, 0);
            this.sketchpad.Name = "sketchpad";
            this.sketchpad.Size = new System.Drawing.Size(800, 450);
            this.sketchpad.TabIndex = 0;
            this.sketchpad.TabStop = false;
            // 
            // MaskLayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.sketchpad);
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MaskLayer";
            this.Opacity = 0.5D;
            this.ShowInTaskbar = false;
            this.Text = "MaskLayer";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.sketchpad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox sketchpad;
    }
}