﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadeGIF.BLL
{
    public partial class Result : Form
    {
        public Result(string path)
        {
            InitializeComponent();
            pictureBox1.ImageLocation = path;
            pictureBox1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Path.GetDirectoryName(pictureBox1.ImageLocation));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (File.Exists(pictureBox1.ImageLocation)) {
                File.Delete(pictureBox1.ImageLocation);
                MessageBox.Show("删除成功!");
                this.Close();
            }
            else
            {
                MessageBox.Show("文件不存在!");
            }
        }
    }
}
