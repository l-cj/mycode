﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace MadeGIF.BLL
{
    public partial class ToolBar : Form
    {
        /// <summary>
        /// 区域选取按钮
        /// </summary>
        public event Action<object, EventArgs> RectangleSelectEvent;
        /// <summary>
        /// 开始录屏按钮
        /// </summary>
        public event Action<object, EventArgs> StartEvent;
        /// <summary>
        /// 停止录屏按钮
        /// </summary>
        public event Action<object, EventArgs> StopEvent;
        /// <summary>
        /// 设置按钮
        /// </summary>
        public event Action<object, EventArgs> SettingEvent;
        /// <summary>
        /// 是否为截图模式 切换事件
        /// </summary>
        public event Action<bool> CheckedChanged;

        public ToolBar()
        {
            InitializeComponent();

            var toolTip1 = new ToolTip();
            toolTip1.SetToolTip(this.checkBox1, @"截图模式，不再录制GIF，只把图片保存在鼠标粘贴板里，像QQ那样");
            toolTip1.SetToolTip(button4, @"按住鼠标左键，拖动进行选择范围；按鼠标右键，放弃范围选择。");
        }
        public void OnMessage(int type,string msg)
        {
            switch (type)
            {
                case 0:
                    BeginInvoke((Action)(() => {
                        this.Text = msg;
                    }));
                    break;
                case 1:
                    BeginInvoke((Action)(() => {
                        label2.Text = msg;
                    }));
                    break;
                case 2:
                    BeginInvoke((Action)(() => {
                        label1.Text = msg;
                    }));
                    break;
            }
            
        }


        private void button4_Click(object sender, EventArgs e)
        {
            RectangleSelectEvent?.Invoke(sender,e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StartEvent?.Invoke(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StopEvent?.Invoke(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SettingEvent?.Invoke(sender, e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckedChanged?.Invoke(checkBox1.Checked);
        }

        private void ToolBar_Load(object sender, EventArgs e)
        {

        }
    }
}
