﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDLC_Report
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            List<ReportModel> reports = new List<ReportModel>();
            for (int i = 0; i < 1000; i++)
            {
                reports.Add(new ReportModel { Name = "名字" + i, Age = i, Genner = "性别" + i });
            }
            stopwatch.Stop();
            Console.WriteLine($"模拟数据生成完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");
            stopwatch.Restart();

            

            var rdlcPath = $"{AppDomain.CurrentDomain.BaseDirectory}MyFirstReport.rdlc";
            var savePath = $"{AppDomain.CurrentDomain.BaseDirectory}File/Image{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            ReportRDLC reportRDLC = new ReportRDLC(rdlcPath,savePath,ReportRDLC.FileType.Image);


            var datasetDic = new Dictionary<string, object>();
            //数据集1 “ReportModel”这个名字需要跟在rdlc模板里定义好的名字相同
            datasetDic.Add("ReportModel", reports);
            //数据集2 “ReportMdoel1”这个名字需要跟在rdlc模板里定义好的名字相同,前面手误打错了单词，也懒得改了
            datasetDic.Add("ReportMdoel1", new List<ReportModel1> { new ReportModel1 { Test1 = "测试", Test2 = 111, Test3 = "测试" } });
            
            reportRDLC.DataSources = datasetDic;

            //自定义参数列表
            var paramList = new List<Microsoft.Reporting.WinForms.ReportParameter>();
            paramList.Add(new Microsoft.Reporting.WinForms.ReportParameter("Title", "XXX报表"));//name 要与rdlc模板里定义好的名字一样
            reportRDLC.ReportParams = paramList;
            
            reportRDLC.DeviceInfo = new ReportDeviceInfo { PageHeight = "20cm" };//定义报表页面大小

            stopwatch.Stop();
            Console.WriteLine($"实例化报表对象完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");
            stopwatch.Restart();

            //输出初始化类型 Image 文件后缀为tif 图片可翻页
            Console.WriteLine(reportRDLC.SaveAsFile());
            Console.WriteLine(reportRDLC.RenderOut.MimeType);
            stopwatch.Stop();
            Console.WriteLine($"生成Image文件完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");
            stopwatch.Restart();


            //修改类型 输出Excel类型 文件后缀为xls
            reportRDLC.SaveType = ReportRDLC.FileType.Excel;
            reportRDLC.SavePath = $"{AppDomain.CurrentDomain.BaseDirectory}File/Excel{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            Console.WriteLine(reportRDLC.SaveAsFile());
            Console.WriteLine(reportRDLC.RenderOut.MimeType);
            stopwatch.Stop();
            Console.WriteLine($"生成Excel文件完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");
            stopwatch.Restart();

            //修改类型 输出Word类型 文件后缀为doc
            reportRDLC.SaveType = ReportRDLC.FileType.Word;
            reportRDLC.SavePath = $"{AppDomain.CurrentDomain.BaseDirectory}File/Word{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            Console.WriteLine(reportRDLC.SaveAsFile());
            Console.WriteLine(reportRDLC.RenderOut.MimeType);
            stopwatch.Stop();
            Console.WriteLine($"生成Word文件完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");
            stopwatch.Restart();

            //修改类型 输出PDF类型 文件后缀为pdf
            reportRDLC.SaveType = ReportRDLC.FileType.PDF;
            reportRDLC.SavePath = $"{AppDomain.CurrentDomain.BaseDirectory}File/PDF{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            Console.WriteLine(reportRDLC.SaveAsFile());
            Console.WriteLine(reportRDLC.RenderOut.MimeType);
            stopwatch.Stop();
            Console.WriteLine($"生成PDF文件完毕，耗时：{stopwatch.ElapsedMilliseconds} ms");

            reportRDLC.Dispose();
            Console.WriteLine($"完毕");
            Console.Read();
        }
    }

    public class ReportModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Genner { get; set; }
    }
    public class ReportModel1
    {
        public string Test1 { get; set; }
        public int Test2 { get; set; }
        public string Test3 { get; set; }
    }
}
