﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTableToEntityClass
{
    public interface IDAL
    {
        List<TempModel> GetDataBaseList(string conStr);
        List<TempModel> GetTableList(string conStr);
        List<TempModel> GetTableColumns(string conStr,string table);
    }
}
