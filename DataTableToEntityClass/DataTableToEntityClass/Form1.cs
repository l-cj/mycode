﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataTableToEntityClass
{
    /// <summary>
    /// MSSQL数据库表 批量转c#实体类 劳承杰 2019年1月3日15:30:51
    /// </summary>
    public partial class Form1 : Form
    {
        BLL _bll;
        string _dataSource = string.Empty;
        string _userID = string.Empty;
        string _pwd = string.Empty;
        public Form1()
        {
            
            InitializeComponent();
            _bll = new BLL();
            progressBar1.Maximum = 100;
            progressBar1.Minimum = 0;
            comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox3.SelectedIndex = 0;
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            textBox4.Text = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\');
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _dataSource = textBox1.Text;
                _userID = textBox2.Text;
                _pwd = textBox3.Text;

                if (comboBox3.Text == "Windows 身份验证" && string.IsNullOrWhiteSpace(_dataSource))
                {
                    MessageBox.Show("请输入服务器地址（eg:192.168.1.1）");
                    return;
                }
                else if (comboBox3.Text == "SQL Server 身份验证" && (string.IsNullOrWhiteSpace(_dataSource) || _userID == null || _pwd == null))
                {
                    MessageBox.Show("请输入服务器地址（eg:192.168.1.1）、登录名和密码");
                    return;
                }

                var dataBase = _bll.GetDataBaseList(getConn("master"));
                comboBox1.DataSource = dataBase;
                comboBox1.DisplayMember = "Name";
                comboBox1.ValueMember = "Name";
                      
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var temp = comboBox1.SelectedValue as TempModel;
            var table = _bll.GetTableList(getConn(temp==null? comboBox1.Text:temp.Name));
            comboBox2.DataSource = table;
            comboBox2.DisplayMember = "Name";
            comboBox2.ValueMember = "Name";
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if(comboBox1.Items.Count==0)
                {
                    MessageBox.Show("没有数据库！");
                    return;
                }
                if (comboBox2.Items.Count == 0)
                {
                    MessageBox.Show("没有表！");
                    return;
                }
                CancellationTokenSource cts = new CancellationTokenSource();
                Task.Factory.StartNew((ct) => {
                    var t = (CancellationToken)ct;
                    while (!t.IsCancellationRequested)
                    {
                        BeginInvoke((Action)(() => {
                            if (progressBar1.Value < 100)
                            {
                                progressBar1.Value++;
                            }
                            else
                            {
                                progressBar1.Value = 0;
                            }
                        }));
                        Thread.Sleep(500);
                    }
                    BeginInvoke((Action)(() => { progressBar1.Value = 100; }));
                },cts.Token);
                var dataBase = comboBox1.Text;
                if (checkBox1.Checked)
                {               
                    _bll.GenerateEntityClass(getConn(dataBase), dataBase, textBox4.Text);
                }
                else
                {
                    var table = comboBox2.Text;
                    _bll.GenerateEntityClass(getConn(dataBase), dataBase, textBox4.Text, table);
                }
                cts.Cancel();           
                MessageBox.Show("操作完成！");
                System.Diagnostics.Process.Start(textBox4.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        string getConn(string dataBase)
        {
            if (comboBox3.Text == "Windows 身份验证")
            {
               return _bll.GetConn(dataBase, _dataSource);
            }
            else
            {
                return _bll.GetConn(dataBase, _dataSource,_userID,_pwd);
            }
                
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "Windows 身份验证")
            {
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                textBox2.ReadOnly = true;
                textBox3.ReadOnly = true;
            }
            else
            {
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                textBox2.ReadOnly = false;
                textBox3.ReadOnly = false;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件保存的位置";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox4.Text = dialog.SelectedPath.Trim();
            }     
        }


    }
}
