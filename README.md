# mycode

#### 介绍
个人项目分享


#### 项目说明

1. 【RDLC_Report】C#报表，使用微软rdlc模板，生成Excel, PDF, Word, Image文件，使用自定义对象做数据集不连接数据库

2. 【UpdateApp】桌面应用更新程序
启动时，根据主程序本地的版本文件（LocalVersion.xml），拿到远程更新地址，比较远程配置的版本文件（ServerVersion.xml）
如果有新版本，则判断更新程序是否位于系统盘，且是否为管理员身份运行
如果位于系统盘，且不是管理员身份运行，则重新以管理员身份运行更新重启，操作系统会弹出账号控制提示给客户
如果不是则打开主窗体，提示有新版本可以更新，是否下载更新
（这个机制可以达到无论客户把程序装在系统盘还是其他盘都不会影响更新操作，并且如果客户不是装在系统盘，还可以免去每次检查更新系统都弹出"权限控制"的提示给客户）
下载更新完成后，会自动删除下载的更新包，并且重新启动主程序

在主程序启动时，同时启动更新程序，即可检查更新了，如果客户选中暂时不更新，下次打开还是会提示更新，如果想跳过该版本，那就得在取消的时候做点操作
把远程最新版本号，修改到主程序本地的版本文件（LocalVersion.xml）


3. 【DataTableToEntityClass】MSSQL数据库表 批量转c#实体类
MSSQL数据库表，批量转c#实体类 ，我知道网上有现成的，可是我就想自己写个来玩玩
这个工具的关键点，在于获取到表的所有字段名和类型，然后就是拼接字符串的事了

4.【MadeGIF】屏幕录制GIF、屏幕截图 2019年2月17日01:10:57
录屏制作GIF的一个小工具
不足：目前还需要一个好的图片压缩方法，不然录制高清的，合成GIF时太耗时了

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)