﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UpdateApp
{
    static class Program
    {       
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.ThreadException += (sender, e) =>
            {
                MessageBox.Show(e.Exception.Message);
            };
            var temp = Update.HasNewVersion();
            if (temp.Result)
            {
                if (Update.IsSystemPath() && ! Update.IsAdministrator())
                {
                    Update.ReStartAppWithAdministrator();                  
                }
                else
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1(temp));
                }                
            }
            else
            {
                Environment.Exit(0);
            }            
        }
    }
}
